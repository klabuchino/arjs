
interface Fields {
    [ key: string ]: Field;
}

interface Row {
    [ key: string ]: any;
}

interface Field {
    /**some fields */
}

class ActiveRecord {
    tableName: string;
    pk: string;
    pkSequenceName: string;
    fields: Fields;
    [key: string]: any;
    pginstance: any;

    constructor() {
        throw 'Cannot create an instance of an abstract class.';
    }

    async save(pginstance: any): Promise<this> {
        
        if (this[this.pk]) {

            let startSql = `UPDATE ${this.tableName} SET `;
            let middleSql = ``;

            let values = [];
            let inc = 1;

            for(let fieldName in this.fields) {
                if(fieldName === this.pk) continue;

                // чтобы поставить поле в бд в null, нужно строго определить поле в null, иначе его не обновляем
                if(this[fieldName] || this[fieldName] === null) {

                    middleSql += ` ${this[fieldName]} = $${inc}, `;
                    values.push(this[fieldName]);
                }

                inc++;
            }

            middleSql = middleSql.slice(0,-1);
            middleSql += ` WHERE ${this.pk} = $${inc++}`;
            middleSql += ` RETURNING * `;
            values.push(this[this.pk]);

            const sqlResult = await this._getPg(pginstance).query( startSql + middleSql, values );
            this.load(sqlResult.rows[0]);

        } else {
            let startSql = `INSERT INTO ${this.tableName}( `;
            let middleSql = `VALUES (`;
            let values = [];
            let inc = 1;
            for (let nameField in this.fields) {
                if (nameField === this.pk) {
                    middleSql += `nextval('${this.pkSequence}'::regclass),`;
                    startSql += `${nameField},`
                    continue;
                }
                startSql += `${nameField},`
                middleSql += `$${inc},`;
                values.push(this[nameField] || null);
                inc++;
            }

            startSql = startSql.slice(0, -1) + ' ) ';
            middleSql = middleSql.slice(0, -1) + `) RETURNING * `;

            const sqlResult = await this._getPg(pginstance).query(startSql + middleSql, values);
            this.load(sqlResult.rows[0]);
        }
        return this;
    }

    async delete(pginstance: any) {
        const sql = `DELETE FROM ${this.tableName} where ${this.pk} = $1`;
        await this._getPg(pginstance).query(sql, [ this[this.pk] ]);
    }

    static async findByPk(pk_id: string | number, pginstance: any) {
        const activeRecord = new this();
        const sql = `SELECT * FROM ${activeRecord.tableName} where ${activeRecord.pk} = $1`;
        const sqlResult = await pginstance.query(sql, [pk_id]);
        if (sqlResult.rows.length) {
            activeRecord.load(sqlResult.rows[0]);            
            activeRecord.pginstance = pginstance;
        } 
        return activeRecord;
    }

    load(params: Row) {
        Object.assign(this, params);
    }

    private _getPg(pginstance: any) {
        // return db instance
            let pg = pginstance;
            if (!pg && !this.pginstance) {
                throw 'empty pginstance';
            }
            pg = this.pginstance;
            return pg;
        }
}

export default ActiveRecord;